﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using System.IO;

namespace kien10engcnt
{
    public static class Utils
    {
        private static readonly HttpClient _client = new HttpClient();
        public static int GetIso8601WeekOfYear(DateTime time)
        {
            // Seriously cheat.  If its Monday, Tuesday or Wednesday, then it'll
            // be the same week# as whatever Thursday, Friday or Saturday are,
            // and we always get those right
            DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
            {
                time = time.AddDays(3);
            }

            // Return the week of our adjusted day
            return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }

        public static async Task<string> DownloadImageAsync(Uri uri)
        {
            var ext = Path.GetExtension(uri.Segments[uri.Segments.Length - 1]);
            var name = $"{Guid.NewGuid()}{ext}";
            using (var fs = new FileStream(Path.Combine("wwwroot", name), FileMode.Create))
            using (var stream = await _client.GetStreamAsync(uri))
            {
                await stream.CopyToAsync(fs);
            }
            return name;
        }
        
        public static async Task<string> GetBase64StringAsync(string uri)
        {
            return Convert.ToBase64String(await _client.GetByteArrayAsync(uri));
        }
    }
}
