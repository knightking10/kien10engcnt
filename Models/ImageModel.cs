﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace kien10engcnt.Models
{
    public class ImageModel
    {
        public string Id { get; set; }
        public string Url { get; set; }
        public string LocalPath { get; set; }
        public bool HasBeenModified { get; set; }

        public string Base64String { get; set; }
    }
}
