﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace kien10engcnt
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();

        public static Dictionary<string, string> SpeakingTopics = new Dictionary<string, string>
        {
            ["A dream school"] = "Dzung",
            ["Is money more important than love?"] = "Việt",
            ["Dreams"] = "Minh Huyền",
            ["Travelling"] = "Minh Anh",
            ["Is online shopping advantageous"] = "Thu Uyên",
            ["Pollution"] = "Thanh Ngân",
            ["Bullying at school"] = "Kiên",
            ["Favourite artist/singer/idol"] = "Hà",
            ["Creativity in schools"] = "Lân",
            ["Disadvantages of social network"] = "Thu Giang",
            ["Holidays"] = "Thúy Ngân",
            ["Future job"] = "Kiều Ngân",
            ["Gender equality"] = "Đào",
            ["Disadvantages of being famous"] = "Ngọc Mai",
            ["Natural wonders of Vietnam"] = "Hiền Mai",
            ["The value of music in life"] = "Thu Trang",
            ["The power of teamwork"] = "Nga",
            ["Drawbacks of festivals"] = "Minh",
            ["Studying overseas"] = "Tuyết Anh",
            ["Body shaming"] = "Hương",
            ["The importance of exams"] = "Vân Anh",
            ["How to find your passion"] = "Hà My",
            ["Advantages of living in city/ countryside"] = "Hiếu",
            ["Favourite film/ movie"] = "Quỳnh Anh",
            ["Mobiles phone at school"] = "Minh Hằng",
            ["Consequences of poor parenting"] = "Huyền Anh",
            ["Pressure teenagers facing nowadays"] = "Nguyệt Linh",
            ["Advantages of learning English"] = "Hà Linh",
            ["Strengths and weaknesses of teenagers"] = "Thu Huyền",
            ["The important of friendship"] = "Hà Phương",
            ["How students can reduce stress"] = "Hiền",
            ["How to live a healthy life"] = "Ngọc Linh",
            ["How teenage pregnancy affects a female student"] = "Châu",
            ["What would the world be like without love?"] = "Hưng",
            ["Describe your favourite means of communication"] = "Mai Phương",
            ["Benefits of doing yoga"] = "Duyên"
        };
    }
}
