﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using kien10engcnt.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.FileProviders;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Processing;

namespace kien10engcnt.Controllers.Utilities
{
    [Route("image")]
    public class ImageRotatorController : Controller
    {
        public IActionResult Index()
        {
            return View("~/Views/Utilities/ImageRotator.cshtml");
        }

        [HttpPost]
        public async Task<IActionResult> Upload(ImageModel image)
        {
            image.Base64String = "data:image;base64," + await Utils.GetBase64StringAsync(image.Url);
            return View("~/Views/Utilities/ImageRotator.cshtml", image);
        }
        [HttpGet]
        [Route("{id}")]
        public IActionResult Rotate90Degrees(string id)
        {
            var imageModel = new ImageModel() { Id = id, LocalPath = $"{id}.jpg", HasBeenModified = true};
            var realPath = $"wwwroot\\{imageModel.LocalPath}";
            using (var image = Image.Load(realPath))
            {
                image.Mutate(ops =>
                {
                    ops.Rotate(RotateMode.Rotate90);
                });
                image.Save(realPath);
            }
            return View("~/Views/Utilities/ImageRotator.cshtml", imageModel);
        }
    }
}