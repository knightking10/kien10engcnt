﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SixLabors.Fonts;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;
using SixLabors.Primitives;
using SixLabors.Shapes;
using System.Net.Http;

namespace kien10engcnt.Controllers.School
{
    [Route("speaking_topics")]
    [Route("SpeakingTopics")]
    public class SpeakingTopicsController : Controller
    {
        public IActionResult Index()
        {
            return View("~/Views/School/SpeakingTopics.cshtml");
        }
         
    }
}